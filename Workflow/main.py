import sys, os
import json
import configparser
import subprocess as sp
from itertools import groupby
from functools import partial
from tools_pipe import pipes
from configparser import ConfigParser, ExtendedInterpolation
from cosmos.api import Cosmos, Dependency, draw_stage_graph, draw_task_graph, pygraphviz_available, default_get_submit_args

def settings(svr,output_dir):
    # Set settings.conf file 
    config = configparser.SafeConfigParser()
    config_file=os.path.join(sys.path[0],'settings.conf')
    config.read(config_file)
    if svr=="aws":
        config.set('opt', 'scratch', '${aws:scratch}')
        config.set('opt', 'share_ref', '${aws:share_ref}')
        config.set('opt', 'share_tools', '${aws:share_tools}')
    elif svr=="local":
        config.set('opt', 'scratch', '${local:scratch}')
        config.set('opt', 'share_ref', '${local:share_ref}')
        config.set('opt', 'share_tools', '${local:share_tools}')
    config.write(open(config_file,'w'))

def get_drmaa_native_specification(task, default_queue='all.q', parallel_env='orte'):
    """
    Default method for determining the extra arguments to pass to the DRM.
    For example, returning `"-n 3" if` `task.drm == "lsf"` would cause all jobs
    to be submitted with `bsub -n 3`.
    :param cosmos.api.Task task: The Task being submitted.
    :param str default_queue: The default_queue to submit to
    :rtype: str
    """
    drm = task.drm
    default_job_priority = None
    use_mem_req = True
    use_time_req = False
    core_req = task.core_req
    mem_req = task.mem_req if use_mem_req else None
    time_req = task.time_req if use_time_req else None
    jobname = '%s[%s]' % (task.stage.name, task.uid.replace('/', '_'))
    default_queue = ' -q %s' % default_queue if default_queue else ''
    priority = ' -p %s' % default_job_priority if default_job_priority else ''
    
    if drm in ['lsf', 'drmaa:lsf']:
        rusage = '-R "rusage[mem={mem}] ' if mem_req and use_mem_req else ''
        time = ' -W 0:{0}'.format(task.time_req) if task.time_req else ''
        return '-R "{rusage}span[hosts=1]" -n {task.core_req}{time}{default_queue} -J "{jobname}"'.format(**locals())
    elif drm in ['ge', 'drmaa:ge']:
        '''        
        h_vmem = int(math.ceil(mem_req / float(core_req))) if mem_req else None
    
        def g():
            resource_reqs = dict(h_vmem=h_vmem, slots=core_req, time_req=time_req)
            for k, v in resource_reqs.items():
                if v is not None:
                    yield '%s=%s' % (k, v)

        resource_str = ','.join(g())
        '''
        return '{queue} -l spock_mem={mem_req}M,num_proc={cpu_req} {priority} -N "{jobname}"'.format(queue=default_queue, mem_req=mem_req, cpu_req=core_req,jobname=jobname,priority=priority)
        #return '-cwd -pe {parallel_env} {core_req} {priority} -N "{jobname}"{queue}'.format(resource_str=resource_str,priority=priority,queue=default_queue,jobname=jobname, core_req=core_req,parallel_env=parallel_env)

    elif drm == 'local':
        return None
    else:
        raise Exception('DRM not supported: %s' % drm)

def recipe(workflow, input_dict, output_dir):
    """
    Input file is a json of the following format:
    [
      {
        "chunk": "001",
        "sample_name": "F1",
        "platform": "ILLUMINA",
        "platform_unit": "FlowCellId",
        "library": "TruSeq_CustomAmplicon",
        "rgid" : "F1",
        "pair": "1",
        "path": "/home/ettore/Scrivania/Fastq/FastqF2_R1_trim.fastq"
      },
      {..}
    ]
    """
    
    # Read config File
    config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
    config.read(os.path.join(sys.path[0],'settings.conf'))
      
    # Load Fatsq files
    input_json = json.load(open(input_dict,'r'))
    load_fastq=[]
    for input in input_json:
        load_fastq.append(workflow.add_task(
                                    func=pipes.Load_Fastq,
                                    params=dict(rgid=input['rgid'], sample_name=input['sample_name'], library=input['library'], 
                                                platform=input['platform'], platform_unit=input['platform_unit'],
                                                chunk=input['chunk'], pair=input['pair'], path=input['path']),
                                    uid="_".join(["LoadFastq",input['sample_name'], input['chunk'],input['pair']])))
    
    # Alignment
    get_chunk = lambda task: {'rgid':task.params['rgid'], 'sample_name':task.params['sample_name'], 'chunk':task.params['chunk'], 'library':task.params['library'], 'platform':task.params['platform'], 'platform_unit':task.params['platform_unit']}
    fastq_task_group={}
    align_fastq=[]
    check_fastq=[]
    count=1
    for tags, load_tasks_group in groupby(load_fastq, get_chunk):
        fastq_task_group[count]=load_tasks_group 
        # fastq_task_group[count] = itertools._grouper object
        # tags = Tags dict
        # parent = Task list
        parent=[]                                   
        for element in fastq_task_group[count]:
            parent.append(element)
        stage_name="FastQC"
        check_fastq.append(workflow.add_task(
                                    func=pipes.Fastqc,
                                    stage_name=stage_name,
                                    params=dict(core_req=config['requirements']['aligment_core_req'], 
                                                mem_req=int(config['requirements']['aligment_mem_req'])*1024, 
                                                in_txts=[Dependency(t, 'path') for t in parent],
                                                out_dir='%s/%s/' %  (output_dir, stage_name ),
                                                out_subdir='%s/%s/%s/%s/' %  (output_dir, stage_name,tags['sample_name'], tags['chunk']),
                                                out_summary='%s/%s/%s_%s.summary.txt' %  (output_dir, stage_name,tags['sample_name'], tags['chunk']),
                                                **tags),
                                    parents=parent,
                                    uid="_".join([stage_name,tags['sample_name'], tags['chunk']])))
        
        stage_name="Alignment"
        align_fastq.append(workflow.add_task(
                                    func=pipes.Alignment,
                                    stage_name=stage_name,
                                    params=dict(core_req=config['requirements']['aligment_core_req'], 
                                                mem_req=int(config['requirements']['aligment_mem_req'])*1024, 
                                                in_txts=[Dependency(t, 'path') for t in parent],
                                                out_bam='%s/%s/%s/%s.bam' %  (output_dir, stage_name, tags['sample_name'], tags['chunk']),
                                                **tags),
                                    parents=parent,
                                    uid="_".join([stage_name,tags['sample_name'], tags['chunk']])))
    
 
    # Group By rgid, sorted by rgid and sample_type
    get_sample = lambda task: {'rgid':task.params['rgid'], 'sample_name':task.params['sample_name']}
    bam_aligned={}
    aligned=[]
    coverage=[]
    for tags, aligned_samples in groupby(align_fastq, get_sample):
        bam_aligned[count]=aligned_samples
        parent=[]                                   
        for element in bam_aligned[count]:
            parent.append(element)
        stage_name="Mark_Duplicates"
        duplicate_task = workflow.add_task(
                                func=pipes.MarkDuplicates,
                                stage_name=stage_name,
                                params=dict(core_req=8, 
                                            mem_req=int(config['requirements']['picard_mem_req'])*1024, 
                                            in_bams=[Dependency(t, 'out_bam') for t in parent],
                                            out_bam = '%s/%s/%s.bam' % (output_dir, stage_name, tags['rgid']),
                                            rgid=tags['rgid']),
                                            parents=parent,
                                uid="_".join(tags['rgid']))
        aligned.append(duplicate_task)

"""  
        
        stage_name="Coverage"
        coverage_task = workflow.add_task(
                                func=pipes.Coverage,
                                stage_name=stage_name,
                                params=dict(core_req=1,
                                            mem_req=int(config['requirements']['bedtools_mem_req'])*1024, 
                                            in_bam=[Dependency(duplicate_task, 'out_bam')],
                                            out_count='%s/%s/%s/%s.count' %  (output_dir, stage_name, tags['rgid'], tags['rgid']),
                                            rgid=tags['rgid']
                                            ),
                                uid="_".join([tags['rgid']]))
        coverage.append(coverage_task)
"""

if __name__ == '__main__':
    import argparse

    p = argparse.ArgumentParser()
    p.add_argument('-drm', default='drmaa:ge', help='', choices=('local', 'drmaa:ge', 'ge'))
    p.add_argument('-svr', default='local', help='', choices=('local', 'aws'))
    p.add_argument('-il', '--input_dict', type=str, required=True)
    p.add_argument('-od', '--output_dir', type=str, required=True)
    args = p.parse_args()
    print args
    
    cosmos = Cosmos('sqlite:///%s/sqlite.db' % os.path.dirname(os.path.abspath(__file__)),
                    # example of how to change arguments if you're NOT using default_drm='local'
                    #get_submit_args=partial(default_get_submit_args, parallel_env='orte'),
                    get_submit_args=get_drmaa_native_specification,
                    default_drm=args.drm)
    cosmos.initdb()
    
    sp.check_call('mkdir -p '+args.output_dir, shell=True)
    os.chdir(args.output_dir)

    settings(args.svr, args.output_dir)
    
    workflow = cosmos.start('Test', restart=False, skip_confirm=True)
    recipe(workflow, args.input_dict, args.output_dir)
    
    workflow.make_output_dirs()
    workflow.run(max_attempts=4)

    if pygraphviz_available:
        # These images can also be seen on the fly in the web-interface
        draw_stage_graph(workflow.stage_graph(), '/tmp/ex1_task_graph.png', format='png')
        draw_task_graph(workflow.task_graph(), '/tmp/ex1_stage_graph.png', format='png')
    else:
        print 'Pygraphviz is not available :('
