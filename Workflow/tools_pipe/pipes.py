import sys, os
import textwrap
import configparser
from configparser import ConfigParser, ExtendedInterpolation

# Read config File
config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','settings.conf'))

def _list2input(l, opt):
    return opt + (" "+opt).join(map(lambda x: str(x), l))

# set -e: If a command fails, set -e will make the whole script exit, instead of just resuming on the next line.
# set -o pipefail: set -o pipefail causes a pipeline (for example, curl -s http://sipb.mit.edu/ | grep foo) to produce a failure return code if any command errors.
# The mktemp utility takes the given filename template and overwrites a portion of it to create a unique filename. 
# Tee command is used to store and view (both at the same time) the output of any other command. 
# Crea una cartella temporanea e si posiziona in questa

cmd_init = textwrap.dedent(r"""
            currentDir=$(pwd) && export CURRENT_DIR=$currentDir;
            tmpDir=$(mktemp -d --tmpdir={config[opt][scratch]}) && export TMPDIR=$tmpDir;
            printf "%s %s at %s\n" "$(date "+%T %D")" "$(hostname)" "$tmpDir" | tee /dev/stderr && cd $tmpDir;
            """.format(config=config))

cmd_out  = textwrap.dedent(r"""
            echo ""$(date "+%T %D")" Moving files to Storage"; 
            [[ -a $tmpDir/out.bam ]] && mv -f $tmpDir/out.bam $OUT_FILE;
            OUT_FILE_BAI=$(echo $OUT_FILE | sed -e "s/.bam/.bai/g");
            [[ -a $tmpDir/out.bai ]] && mv -f $tmpDir/out.bai $OUT_FILE_BAI;
            cd && /bin/rm -rf $tmpDir;
            """)

def awk_cmd():
    return """xargs awk -F'\t' '$1 == "Filename" {{sub("_[0-9]*.fastq",".fastq",$2); sample=$2}}; $1 == "Total Sequences" {{test=$2"\t"$1"\t"sample}}; END {{print test}}' """

def Load_Fastq(rgid, chunk, library, platform, sample_name, pair, path, platform_unit):
    return
  
def Fastqc(core_req, mem_req, in_txts, rgid, sample_name, platform, platform_unit, chunk, library, out_dir, out_subdir, out_summary):
    
    cmd_main = textwrap.dedent(r"""
        outDIR={out_dir} && export OUT_DIR=$outDIR; 
           
        {config[tools][fastqc]} \
        -d $tmpDir \
        -f fastq \
        --nogroup \
        --casava \
        -t 4 \
        -o $tmpDir \
        {in_txts[0]} \
        {in_txts[1]} \
        && \
        unzip $tmpDir/\*.zip -d $tmpDir \
        && \
        rm $tmpDir/*.zip \
        && \
        cp -R $tmpDir/* $outDIR \
        && \
        cp -R $tmpDir/* {out_subdir} \
        && \
        find {out_subdir} -type f -name 'summary.txt' | xargs cat > {out_summary} \
        && \
        for file in $(find {out_subdir} -type f -name 'fastqc_data.txt');
        do {awk} $file >> {out_summary};
        done;
        """.format(config=config,
                   awk=awk_cmd(),
                   **locals()))

    return cmd_init+cmd_main

def Alignment(core_req, mem_req, in_txts, rgid, sample_name, platform, platform_unit, chunk, library, out_bam):
    
    cmd_main = textwrap.dedent(r"""
        outFile={out_bam} && export OUT_FILE=$outFile; 
           
        {config[tools][bwa]} mem \
        -M \
        -t {core_req} \
        -R "@RG\tID:{rgid}\tLB:{library}\tSM:{sample_name}\tPL:{platform}\tPU:{platform_unit}" \
        {config[GRCh37][reference_fasta]} \
        {in_txts[0]} \
        {in_txts[1]} \
        | \
        {config[tools][java]} -Xmx{mem_req}M -jar {config[tools][picard_dir]}/AddOrReplaceReadGroups.jar \
        INPUT=/dev/stdin \
        OUTPUT=/dev/stdout \
        RGID={rgid} \
        RGLB={library} \
        RGSM={sample_name} \
        RGPL={platform} \
        RGPU={platform_unit} \
        COMPRESSION_LEVEL=0 \
        | \
        {config[tools][java]} -Xmx{mem_req}M -jar {config[tools][picard_dir]}/CleanSam.jar \
        INPUT=/dev/stdin \
        OUTPUT=/dev/stdout \
        VALIDATION_STRINGENCY=SILENT \
        COMPRESSION_LEVEL=0 \
        | \
        {config[tools][java]} -Xmx{mem_req}M -jar {config[tools][picard_dir]}/SortSam.jar \
        INPUT=/dev/stdin \
        OUTPUT=out.bam \
        SORT_ORDER=coordinate \
        CREATE_INDEX=True
        """.format(config=config,
                   **locals()))

    return cmd_init+cmd_main+cmd_out

def MarkDuplicates(core_req, mem_req, in_bams, rgid , out_bam):
        
    cmd_main = textwrap.dedent(r"""
        outFile={out_bam} && export OUT_FILE=$outFile; 
        
        {config[tools][java_MD]} -Xmx{mem_req}M -jar {config[tools][picard_dir]}/MarkDuplicates.jar \
        TMP_DIR=$tmpDir \
        OUTPUT=out.bam \
        METRICS_FILE=out.metrics \
        ASSUME_SORTED=True \
        CREATE_INDEX=True \
        MAX_RECORDS_IN_RAM=1000000 \
        MAX_SEQUENCES_FOR_DISK_READ_ENDS_MAP=1000 \
        MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
        VALIDATION_STRINGENCY=SILENT \
        VERBOSITY=INFO \
        {inputs}
        """.format(config = config,
                   inputs = _list2input(in_bams,"INPUT="),
                   **locals()))
    
    return cmd_init+cmd_main+cmd_out